function createCard(name, description, pictureUrl, dates, location) {
  return `
    <div class="col">
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer bg-transparent border-success">${dates}</div>
      </div>
    </div>
  `;
}

function formatDate(date) {
  let eventDate = new Date(date);
  let month = eventDate.getMonth() + 1;
  let day = eventDate.getDate();
  let year = eventDate.getFullYear();
  return `${month}/${day}/${year}`;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error("Response not ok");
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const dates = `${formatDate(startDate)} - ${formatDate(endDate)}`;
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, dates, location);
            const row = document.querySelector(".row, .row-cols-1, .row-cols-md-3, .g-4");
            row.innerHTML += html;
          } else {
            const html = '<div class="alert alert-secondary" role="alert">Conference details could not be loaded</div>';
            const row = document.querySelector(".row, .row-cols-1, .row-cols-md-3, .g-4");
            row.innerHTML += html;
          }
        }
      }
    } catch (e) {
        const html = '<div class="alert alert-danger" role="alert">Our conference schedule is not loading.  Please check back!</div>';
        const row = document.querySelector(".row, .row-cols-1, .row-cols-md-3, .g-4");
        row.innerHTML += html;
        console.error("error", e);
    }

  });
